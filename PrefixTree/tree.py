

class Node():

    def __str__(self):
        return self.char

    def __repr__(self):
        return 'Object of ' + self.char

    def __init__(self, char):
        self.char = char
        self.next = {}


class Tree():
    end_token = "<END>"

    def __init__(self):
        self.tree_head = Node('<HEAD>')

    def addWord(self, word):
        current_node = self.tree_head
        for char in list(word) + [self.end_token]:
            if char not in current_node.next:
                current_node.next[char] = Node(char)
                # print(current_node, ':',current_node.next)
            current_node = current_node.next[char]

    def search(self, word):
        current_node = self.tree_head
        for char in list(word) + [self.end_token]:
            if char not in current_node.next:
                return False
                # print(current_node, ':',current_node.next)
            current_node = current_node.next[char]
        return True


out_string = ''


def writeLine(node, i):
    global out_string
    out_string += '+ '*i + node.char + ': ' + str([char + ' ' for char in node.next]) + '\n'
    for char in node.next:
        if "<END>" not in char: 
            writeLine(node.next[char], i+1)


def prefixTree(input_string):
    words = input_string.split()
    tree = Tree()
    for word in words:
        tree.addWord(word)
    with open('dict.txt', 'w+') as f:
        # string = ''
        writeLine(tree.tree_head, 0)
        f.write(out_string)


# prefixTree('Yasuo Yard Yale')
