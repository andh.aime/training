import torch
import matplotlib.pyplot as plt
from PIL import Image
import numpy as np

img = Image.open("airu.jpg").convert("RGB")
img = img.resize((64, 64), Image.ANTIALIAS)
img = torch.from_numpy(np.array(img))
plt.imshow(img)


def distance(x, y):
    return torch.sum((x-y).pow(2))


def get_center(x, centers):
    min = 10000000
    min_index = 1000000
    for index, center in enumerate(centers):
        if (distance(x, center) < min):
            min_index = index
            min = distance(x, center)
    one_hot = torch.zeros(1, len(centers))
    one_hot[0][min_index] = 1
    return (one_hot, min)


centers = []
def kmeans(input_img, center_num, iteration=1000):
    it = 0
    labels = torch.zeros(input_img.shape[0], center_num)
    # 1.init random center
    new_centers = []
    for i in range(center_num):
        new_centers.append(torch.randint(
            0, 255, input_img[0].shape).type(torch.LongTensor))
    centers.append(new_centers)
    print(1)
    # 4.check if out of iter or new center == old center
    while (it < iteration and not (len(centers) > 1 and [tuple(x) for x in centers[-1]]
                                   == [tuple(x) for x in centers[-2]])):
        it += 1
        # 2.divide points into section
        print(2)
        for i in range(input_img.shape[0]):
            label, _ = get_center(input_img[i], centers[-1])
            labels[i] = label
        # 3.from divided section re-assign center

        print(3)
        for i in range(center_num):
            filter_tensor = labels[:, i].view(-1, 1).type(torch.LongTensor)
            print("torch.sum(filter_tensor, dim=0)",
                  torch.sum(filter_tensor, dim=0))
            new_centers.append(torch.sum(input_img * filter_tensor, dim=0)
                               / (torch.sum(filter_tensor, dim=0) + 1))
            # print(new_centers)


kmeans(img.view(-1, 3).type(torch.LongTensor), 3, 100)
