
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

int main()
{
    struct sockaddr_storage their_addr;
    struct addrinfo hints, *servinfo;
    int s, new_s;
    socklen_t addr_size;
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    getaddrinfo("localhost", "3459", &hints, &servinfo);
    s = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
    bind(s, servinfo->ai_addr, servinfo->ai_addrlen);
    listen(s, 8);
    addr_size = sizeof their_addr;
    new_s = accept(s, (struct sockaddr *)&their_addr, &addr_size);
    char msg[1000] = "unmei wo shinjite";
    int bytes = send(new_s, msg, strlen(msg), 0);
    printf("sent %d bytes\n", bytes);
}