
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netinet/in.h>

int main()
{
    struct addrinfo hints, *res, *p;
    struct sockaddr_storage their_addr;
    int sockfd;
    char msg[100];
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;    //auto 4 6
    hints.ai_socktype = SOCK_DGRAM; //tcp
    hints.ai_flags = AI_PASSIVE;    //automatically find ip
    getaddrinfo(NULL, "4950", &hints, &res);
    // for (p = res; p != NULL; p = p->ai_next)
    // {
    if ((sockfd = socket(res->ai_family, res->ai_socktype,
                         res->ai_protocol)) == -1)
    {
        perror("listener: socket");
        // continue;
    }

    if (bind(sockfd, res->ai_addr, res->ai_addrlen) == -1)
    {
        close(sockfd);
        perror("listener: bind");
        // continue;
    }

    // break;
    // }

    // if (p == NULL)
    // {
    //     fprintf(stderr, "listener: failed to bind socket\n");
    //     return 2;
    // }

    // connect(sockfd, res->ai_addr, res->ai_addrlen);
    // recv(sockfd, &msg, sizeof(msg), 0);

    // udp

    socklen_t addr_len = sizeof their_addr;
    printf("listener: waiting to recvfrom...\n");
    recvfrom(sockfd, &msg, 99, 0, (struct sockaddr *)&their_addr, &addr_len);
    char s[INET6_ADDRSTRLEN];
    printf("inet: %s\n", inet_ntop(their_addr.ss_family, &(((struct sockaddr_in *)&their_addr)->sin_addr), s, sizeof s));
    printf("received: %s\n", msg);
    return 0;
}