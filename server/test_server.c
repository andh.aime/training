
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netinet/in.h>

int main()
{
    char *msg = "blooming lily";
    struct addrinfo hints, *res, *p;
    int sockfd, new_fd, bytes_sent;
    char s[INET6_ADDRSTRLEN];

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;
    // hints.ai_flags = AI_PASSIVE;
    if (getaddrinfo("localhost", "4950", &hints, &res) != 0)
    {
        fprintf(stderr, "getaddrinfo:");
        return 1;
    }

    // sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    // for (p = res; p != NULL; p = p->ai_next)
    // {
    if ((sockfd = socket(res->ai_family, res->ai_socktype,
                         res->ai_protocol)) == -1)
    {
        perror("talker: socket");
        // continue;
    }

    //     break;
    // }

    // bind(sockfd, res->ai_addr, res->ai_addrlen);

    //tcp
    // listen(sockfd, 8);

    // int addr_size = sizeof their_addr;
    // new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &addr_size);
    // printf("%d has connected \n", new_fd);

    // bytes_sent = send(new_fd, msg, strlen(msg), 0);

    //udp
    // while (0)
    // {
    if ((bytes_sent = sendto(sockfd, msg, strlen(msg), 0,
                             p->ai_addr, p->ai_addrlen)) == -1)
    {
        perror("talker: sendto");
        return 1;
    }
    printf("sent %d bytes\n", bytes_sent);
    // }

    return 0;
}