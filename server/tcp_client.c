
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

int main()
{
    int s;
    struct addrinfo hints, *servinfo;
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    getaddrinfo("localhost", "3459", &hints, &servinfo);
    s = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
    connect(s, servinfo->ai_addr, servinfo->ai_addrlen);
    char msg[100];
    printf("%d %d", sizeof(msg), strlen(msg));
    recv(s, msg, sizeof(msg), 0);
    printf("received %s\n", msg);
}