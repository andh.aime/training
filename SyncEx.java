import java.util.Random;

public class SyncEx {


    public static void main(String[] args) {

        Store store = new Store();
//        Producer producer = new Producer();
//        Consumer consumer = new Consumer();
//        consumer.start();
//        producer.start();
//        Barber barber = new Barber();
//        barber.start();
//        Consumer consumer2 = new Consumer();
//        consumer2.start();

        for (int i = 0; i < 3; i++) {
//            Phil phil = new Phil(store, i, (i + 1) % 5);
//            phil.start();
//            Consumer consumer2 = new Consumer();
//            consumer2.start();
//            Producer producer = new Producer();
//            producer.start();
//            Consumer consumer2 = new Consumer();
//            consumer2.start();

            Customer customer = new Customer();
            customer.start();
        }
    }
}

class Store {
    int bufferSize = 3;

    int[] forks = {1, 1, 1, 1, 1};


    volatile int count = 0;
    volatile int appending = 1;
    volatile int taking = 1;

    volatile int mutex = 1;
    volatile int reading = 0;

    volatile int seats = 3;
    volatile int barber = 0;
    volatile int customer = 1;


    public static Store store = new Store();

    void append() {
        while (count >= bufferSize || appending < 1) ;
        appending--;
        System.out.println(count);
        this.count++;
        appending++;
    }

    void take() {
        while (count <= 0 || taking < 1) ;
        taking--;
        System.out.println(count);
        this.count--;
        taking++;
    }

    void writing() {
        while (mutex < 1) ;
        mutex--;
        System.out.println("writing");
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mutex++;
    }

    void reading() {
//        while (true) {
        while (mutex < 1) ;
        System.out.println("reading");
//        }
    }

    volatile int counter = 0;
    volatile int flag = 0;
    volatile int lock = 1;

    void runIntoBarrier() {

        while (mutex < 1);
        mutex--;
        counter++;
        System.out.println(counter);
        if (counter >= 3) {
            lock = 0;
            counter = 0;
            mutex++;
        } else {
            mutex++;
            while (lock > 0) ;
        }

//        System.out.println("kaihou");
    }
}

class Producer extends Thread {

//    Store store;
//
//    public Producer(Store store) {
//        this.store = store;
//    }


    @Override
    public void run() {
        while (true) {
//            while (store.res[1] < 1) ;
//            store.res[1]--;
//            while (store.res[3] < 1) ;
//            store.res[3]--;
//            store.res[2]++;
//            store.res[3]++;
//            System.out.println("Printingu" + store.res[2]);
//            store.res[0]++;
//            System.out.println("full" + store.res[0]);
            Store.store.append();
//            System.out.println(Store.store.count);
//            Store.store.writing();

        }
//        System.out.println("nothing to lose but our chains " + store.count);
//
//        store.append();

    }
}

class Consumer extends Thread {

//    Store store;
//
//    public Consumer(Store store) {
//        this.store = store;
//    }

    @Override
    public void run() {
        while (true) {
//            while (store.res[0] < 1) ; // full
//            store.res[0]--;
//            while (store.res[3] < 1) ; //mutex
//            store.res[3]--;
//            store.res[2]--;
//            store.res[3]++;
//            System.out.println("The void " + store.res[2]);
//            store.res[1]++;
//            System.out.println("empty " + store.res[1]);
            Store.store.take();
//            System.out.println(Store.store.count);
//            Store.store.reading();
        }
//        store.take();
    }
}

class Phil extends Thread {
    Store store;
    int a;
    int b;

    public Phil(Store store, int a, int b) {
        this.store = store;
        this.a = a;
        this.b = b;
    }

    @Override
    public void run() {
        while (true) {
            System.out.println(this.getId() + " is thinking emoji ");
            int max = a > b ? a : b;
            int min = a < b ? a : b;
            while (store.forks[min] < 1) ;
            store.forks[min]--;
            while (store.forks[max] < 1) ;
            store.forks[max]--;
            System.out.println(this.getId() + " zzz");
            store.forks[max]++;
            store.forks[min]++;
        }
    }
}

class Monitor {
    int[] page = {1};

}


class Barber extends Thread {
    @Override
    public void run() {
        while (true) {
            while (Store.store.customer < 1 || Store.store.barber < 1) ;
            Store.store.customer--;
            Store.store.barber--;
            while (Store.store.mutex < 1) ;
            Store.store.mutex--;
            Store.store.seats++;
            System.out.println(Store.store.seats);
            Store.store.mutex++;
            //            Store.store.barber++;
        }
    }
}

class Customer extends Thread {
    @Override
    public void run() {
        while (true) {
//            if (Store.store.seats >= 1) {
//                while (Store.store.mutex < 1) ;
//                Store.store.mutex--;
//                Store.store.seats--;
//                System.out.println(Store.store.seats);
//                Store.store.mutex++;
//                Store.store.customer++;
//                while (Store.store.barber > 0) ;
//                Store.store.barber++;
//            }
            Store.store.runIntoBarrier();
        }
    }
}

