
arr = [0, 2, 3, 5, 1, 3, 3, 5, 4, 2, 10, 4, 0]

def quickSort(low, high):
    if low < high:
        pi = partition(low, high)
        quickSort(low, pi - 1)
        quickSort(pi+1, high)


def partition(low, high):
    i = low
    for j in range(low, high):
        if (arr[j] <= arr[high]):
            arr[j], arr[i] = arr[i], arr[j]
            i += 1
    arr[high], arr[i] = arr[i], arr[high]
    return i

quickSort(0, len(arr)-1)
print(arr)
