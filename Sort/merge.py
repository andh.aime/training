
test = [0,2,3,5,1,3,3,5,4,2,10,4,0]
def mergeSort(arr):
    a = arr[:len(arr)//2] #1,0 a=1 b=2 
    b = arr[len(arr)//2:] #2,3 
    if len(arr) > 2:
        a = mergeSort(a)
        b = mergeSort(b)
    arr = merge(a,b)
    return arr

def merge(a,b):
    c = []
    while a and b:
        if a[0] >= b[0]:
            c.append(b.pop(0))
        else :
            c.append(a.pop(0))
    while a:
        c.append(a.pop(0))
    while b:
        c.append(b.pop(0))
    return c


print(mergeSort(test))